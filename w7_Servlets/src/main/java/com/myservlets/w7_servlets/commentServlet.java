package com.myservlets.w7_servlets;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
//here we have the ability to call the servlet and idenitfiy it with out going and pathing it out in my xml files
@WebServlet(name = "commentServlet", value = "/commentServlet")
public class commentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //my get parameters will record information taken from the index.html form.
        //and then they also creat a string to process that information.
        String name = request.getParameter("userName");
        String comments = request.getParameter("comment");

        response.setContentType("text/html");
        //the writer allows me the ability to creat print statements in servlet
        PrintWriter out = response.getWriter();


        // these print statements act as a way to build my html page so i can be visualized in that way.
        out.println("<html>");
        out.println("<body>");

        out.println("<h1>Comments</h1>");
        // these printstatments are calling my string data that was harvested from my form
        out.println("<p>User Name:"+name+"</p>");
        out.println("<p>Comment:"+"<br>"+comments+"</p>");

        out.println("</body>");
        out.println("</html>");
    }

    // this do post will intiate instead if my servlet request above fails.
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        out.println("Failed");

    }

}
