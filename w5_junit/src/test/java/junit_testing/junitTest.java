package junit_testing;

import org.junit.Test;

import static org.junit.Assert.*;

public class junitTest {

    @Test
    public void testInt() {
        // i creat a new junit class called "test"
        junit test = new junit();
        // i create a assert statement telling my program that i am looking for my "junit" class to equal 3
        // if it does not equal 3 it will send a error then it will tell me what is expected.
        // in "junit" class we have 10/5 which will = 2 to correct my test ithen need to assign n1 = 9 and n2 =3.
        assertEquals(3, test.main(10, 5));

        System.out.println("test success");


    }
    @Test(expected = IllegalArgumentException.class)
    public void testException(){
        junit test = new junit();
        test.main(10, 0);
        System.out.println("test success");
    }
}