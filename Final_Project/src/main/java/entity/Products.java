package entity;

import javax.persistence.*;

@Entity
public class Products {

    @Id
    @GeneratedValue
    @Column(nullable = false)
    private int pid;
    private String productName;
    private String quantityAmount;

    public Products() {

    }

    public Products(int pid, String productName, String quantityAmount) {
        super();
        this.pid = pid;
        this.productName = productName;
        this.quantityAmount = quantityAmount;

    }

    public int getPid() {
        return pid;
    }
    public void setPid(int pid) {
        this.pid = pid;
    }
    public String getProductName() {
        return productName;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }
    public String getQuantityAmount() {
        return quantityAmount;
    }
    public void setQuantityAmount(String quantityAmount) {
        this.quantityAmount = quantityAmount;
    }

}
