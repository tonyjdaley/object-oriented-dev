import entity.Products;

import javax.persistence.*;

public class Main {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();


        transaction.begin();

        Products products = entityManager.find(Products.class, 6);
        System.out.println("products pid :: " + products.getPid());
        System.out.println("products name :: " + products.getProductName());
        System.out.println("products amount :: " + products.getQuantityAmount());

        /*products.setProductName("Gallon_Milk");*/
        products.setQuantityAmount("1");

        entityManager.getTransaction().commit();


        entityManager.close();
        entityManagerFactory.close();
    }
}