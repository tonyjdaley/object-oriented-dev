package com.daley.final_project;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

import entity.Products;
import javax.persistence.*;


@WebServlet(name = "product4", value = "/product4")
public class product4 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Entity manager factory
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        //Input value
        Integer number = Integer.valueOf(request.getParameter("p4Set"));


        response.setContentType("text/html");
        //print Writer for HTML
        PrintWriter out = response.getWriter();


        //HTML build
        out.println("<html>");
        out.println("<link href=\"style.css\" rel=\"stylesheet\" media=\"all\">");
        out.println("<body>");

        out.println("<h1>Total</h1>");
        // Checking input value
        out.println("<p>Amount Set:"+number+"</p>");
        out.println("<main>");
        //Entity Transaction Begins
        try {
            transaction.begin();

            Products products = entityManager.find(Products.class, 4);
            products.setQuantityAmount(String.valueOf(number));
            out.println("<p>Product ID:"+products.getPid()+"</p>");
            out.println("<p>Product Name:"+products.getProductName()+"</p>");
            out.println("<p>Products Total:"+products.getQuantityAmount()+"</p>");
            //Entity Transaction commits and ends
            entityManager.getTransaction().commit();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
        out.println("</main>");
        //HMTL tag close
        out.println("<footer>\n" +
                "    <p class=\"copy-rights\">&copy; 2022 | Anthony J Daley | Soldotna, AK |\n" +
                "        <a href=\"https://www.byui.edu/online\" target=\"_blank\">BYUI Online Learning</a>\n" +
                "    </p>\n" +
                "</footer>");
        out.println("</body>");
        out.println("</html>");
    }

    // Do post if fail
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        out.println("Failed");

    }

}
