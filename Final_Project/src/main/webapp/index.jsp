<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en-us">
<head>
    <title>Tony's Inventory</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src=""></script>
    <link href="style.css" rel="stylesheet" media="all">
</head>
<body>
<header>
    <h1>Products Inventory</h1>
    <nav>
        <a href="index.html">index.html</a>
    </nav>
</header>

<main>

    <form action="commentServlet" method="get" >
        <input type="number" name="userName">
        <input type="submit">
    </form>
</main>

<footer>
    <p class="copy-rights">&copy; 2022 | Anthony J Daley | Soldotna, AK |
        <a href="https://www.byui.edu/online" target="_blank">BYUI Online Learning</a>
    </p>
</footer>
</body>
</html>