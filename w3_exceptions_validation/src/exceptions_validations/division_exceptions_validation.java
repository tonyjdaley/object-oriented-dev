package exceptions_validations;

import java.util.Scanner;

public class division_exceptions_validation {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n1 = 0;
        int n2 = 0;
        boolean number;


        do {
            System.out.print("Input first Number:");
            if (input.hasNextInt()) {
                n1 = input.nextInt();
                number = true;
            } else {
                System.out.println("invalid Entry");
                number = false;
                input.next();
            }
        } while (!(number));


        do {
            System.out.print("Input Second Number:");
            if (input.hasNextInt()) {
                n2 = input.nextInt();
                number = true;
            } else {
                System.out.println("invalid Entry");
                number = false;
                input.next();
            }
        } while (!(number));

        try {
            System.out.println("Solution =" + (n1 / n2));
        } catch (ArithmeticException e) {
            System.out.println("Division by zero is impossible, please try again");

            do {
                System.out.print("Input first Number:");
                if (input.hasNextInt()) {
                    n1 = input.nextInt();
                    number = true;
                } else {
                    System.out.println("invalid Entry");
                    number = false;
                    input.next();
                }
            } while (!(number));

            do {
                System.out.print("Input first Number:");
                if (input.hasNextInt()) {
                    n2 = input.nextInt();
                    number = true;
                } else {
                    System.out.println("invalid Entry");
                    number = false;
                    input.next();
                }
            } while (!(number));

            try {
                System.out.println("Solution =" + (n1 / n2));
            } catch (ArithmeticException s) {
                System.out.println("Division by zero is impossible, Program end");

                //float s = (n1/n2);
                //ystem.out.println("Solution =" + (s));
            }
        }
    }
}
