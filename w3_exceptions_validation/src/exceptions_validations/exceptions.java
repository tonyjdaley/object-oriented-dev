package exceptions_validations;

public class exceptions {
    public static void main(String[] args)
    {
        //exceptions are actually automatically generated due to certain items inside the code not working correctly
        int n1 = 5;
        int n2 = 0;
        try {
            //this is the exception because b = 0 and 0 isnt a int it wont work
            System.out.println(n1 / n2);
        }
        //this catch will catch that exception and input something else instead.
        catch (ArithmeticException e) {

            System.out.println(
                    "Division by zero is impossible");
        }
    }
}



