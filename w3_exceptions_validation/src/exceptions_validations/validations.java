package exceptions_validations;

import java.util.Scanner;

public class validations {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        // these int give n1 and n2 place holding value
        int n1 = 0;
        int n2 = 0;
        // "number" Boolean allows me to use the if else statement, making "number" a checking mechanic of sorts
        // booleans are important for if statements.
        boolean number;

        // this "do" statement acts as a loop or module to hold my logic for my first number input
        do {
            System.out.print("Input first Number:");
            //this if statement validates that my input is an actaul number with "input.hasNextInt"
            if (input.hasNextInt()) {
                //it then makes n1 = to the input if it is a number
                n1 = input.nextInt();
                //then it makes the statement true ending the loop
                number = true;
                // this else tells me wht to try instead if above statement is not true
            } else{
                System.out.println("invalid Entry");
                number = false;
                // very important this input.next allows the loop to end otherwise it will keep printing invalid entry.
                input.next();
            }
        } while (!(number));


        do {
            System.out.print("Input Second Number:");
            if (input.hasNextInt()) {
                n2 = input.nextInt();
                number = true;
            } else{
                System.out.println("invalid Entry");
                number = false;
                input.next();
            }
        } while (!(number));

        float s = (n1/n2);
        System.out.println("Solution =" + (s));
    }
}
