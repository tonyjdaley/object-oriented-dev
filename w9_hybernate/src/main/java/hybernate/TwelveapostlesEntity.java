package hybernate;

import javax.persistence.*;

@Entity
@Table(name = "twelveapostles", schema = "test1", catalog = "")
public class TwelveapostlesEntity {
    @Basic
    @Column(name = "firstName")
    private String firstName;
    @Basic
    @Column(name = "lastName")
    private String lastName;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "apostleNum")
    private Object apostleNum;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Object getApostleNum() {
        return apostleNum;
    }

    public void setApostleNum(Object apostleNum) {
        this.apostleNum = apostleNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TwelveapostlesEntity that = (TwelveapostlesEntity) o;

        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (apostleNum != null ? !apostleNum.equals(that.apostleNum) : that.apostleNum != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (apostleNum != null ? apostleNum.hashCode() : 0);
        return result;
    }
}
