package hybernate;

import javax.persistence.*;

@Entity
@Table(name = "prophets", schema = "test1", catalog = "")
public class ProphetsEntity {
    @Basic
    @Column(name = "firstName")
    private String firstName;
    @Basic
    @Column(name = "lastName")
    private String lastName;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "prophetNum")
    private Object prophetNum;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Object getProphetNum() {
        return prophetNum;
    }

    public void setProphetNum(Object prophetNum) {
        this.prophetNum = prophetNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProphetsEntity that = (ProphetsEntity) o;

        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (prophetNum != null ? !prophetNum.equals(that.prophetNum) : that.prophetNum != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (prophetNum != null ? prophetNum.hashCode() : 0);
        return result;
    }
}
