package hybernate;



import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;


public class main {
    public static void main(String[] args) {
        try {
            EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            EntityTransaction transaction = entityManager.getTransaction();

            transaction.begin();

            ProphetsEntity russel = new ProphetsEntity();
            russel.setProphetNum(8);
            russel.setFirstName("Russel");
            russel.setLastName("M. Nelsom");
            entityManager.persist(russel);

            entityManager.getTransaction().commit();
            entityManager.close();
            entityManagerFactory.close();

        } catch (Exception e) {
            System.out.println("failed");
        }
    }
}
