package com.json;

import java.io.IOException;
import com.fasterxml.jackson.databind.ObjectMapper;
// I am guessing this isn't technically needed because they are in the same package
// import com.json.game_file_info;

public class callup {
    public static void main(String[] a){

        game_file_info gameInfo = new game_file_info();
        // the object mapper is the most important part of conversion, it keeps the
        // information stored and traced so it can be called up.
        ObjectMapper mapObj = new ObjectMapper();
        //validation
        try {
            // this simple function calls up the converted info then assigns it the value of
            // json, and converts store data as a string using the jackson library
            String json = mapObj.writeValueAsString(gameInfo);
            // this then prints the
            System.out.println(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
