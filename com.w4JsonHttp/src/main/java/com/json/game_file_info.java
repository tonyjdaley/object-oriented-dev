package com.json;

public class game_file_info {
    private int gameStoreID = 4531;
    private String gameName = "Breath of the wild";
    private String gameGenre = "Open World";
    private String gameSubGenre = "Fantasy";
    private int rating = 5;
    private int sales = 1000;

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("\nGames store ID number: ").append(gameStoreID);
        sb.append("\nGames name: ").append(gameName);
        sb.append("\nGenre: ").append(gameGenre);
        sb.append("\nSub Genre: ").append(gameSubGenre);
        sb.append("\nRating: ").append(rating);
        sb.append("\nsales: ").append(sales);
        return sb.toString();
    }
    public int getGameID() {
        return gameStoreID;
    }
    public void setGameID(int empId) {
        this.gameStoreID = empId;
    }
    public String getGameName() {
        return gameName;
    }
    public void setGameName(String name) {
        this.gameName = name;
    }
    public String getGenre() {
        return gameGenre;
    }
    public void setGenre(String designation) {
        this.gameGenre = designation;
    }
    public String getSubGenre() {
        return gameSubGenre;
    }
    public void setSubGenre(String department) {
        this.gameSubGenre = department;
    }
    public int getRating() {
        return rating;
    }
    public void setRating(int salary) {
        this.rating = salary;
    }
    public int getSales() {
        return sales;
    }
    public void setSales(int salary) {
        this.sales = salary;
    }
}
