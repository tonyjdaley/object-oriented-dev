package java_collections;

import groovy.util.OrderBy;
//imports will take information stored in a package. in this case the Utility package and us it for data organizing.
//where the "*" is located in the import statement is the file specifcally pulled up, the "*" means all classes inside
import java.util.*;
import java.util.concurrent.PriorityBlockingQueue;

public class queue_collections {
    public static void main(String args[]) {

        // below we are creating a Queue that is told to use a Interger for its data type.
        // then telling it to order it by "PriorityBlockingQueue", there is also "PriorityBlockingQueue","PriorityQueue"
        // q_order is our defining identifier
        Queue<Integer> q_order
            = new LinkedList<>();
        //the "for" statement below is simply us telling a parameter for data that will be stored logically in q
        // then q++ will keep adding on to the queue from 0 until it reaches a max of 4 which is the set parameter
        //logical statements like this allow us to add multiply things to our collection
        for (int q = 0; q < 4; q++)
            //we then use .add to add the information into our Queue
            q_order.add(q);
        //the standard way to add would simply be the "identifier".add(item_to_be_added)
        q_order.add(4);
        q_order.add(5);
        q_order.add(6);
        q_order.add(7);
        //println will take the entire line it was printed on
        System.out.println("println separates lines:"+q_order);
        System.out.println("these first 2 are seperated:"+q_order);
        // normal print will cause the item to stack in the same line
        System.out.print("print by itself just puts it down:"+q_order);
        System.out.println(q_order);
        // we can remove items from the lis with a .remove()
        System.out.println(".remove() takes item away, was item removed:"+q_order.remove(3));
        System.out.println("Item 3 is gone"+q_order);
        //.peek() will show us the item at the top of the list
        System.out.println("first in line is:"+q_order.peek());
        //.size() will show us how many values in the queue
        //there are 7 because we removed 3 ;)
        System.out.println("there are "+q_order.size()+" values left");

        //Queue java collections

    }
}
