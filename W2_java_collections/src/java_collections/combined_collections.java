package java_collections;
//customer callback list and queue

import java.util.*;

public class combined_collections {
    public static void main(String[] args) {
        //List Data collection

        //TreeSet Data collection


        //Map and Queue Data collection
        //For this segment I decided to use a LinkedHashMap, as they seem to hold capabilities of both Queue and Map
        //Hope That fills the Criteria
        LinkedHashMap<String, Integer> map
                = new LinkedHashMap<>();
        map.put("#1 " + "Name:" + "Sarah", 907124532);
        map.put("#2 " + "Name:" + "Matt", 907122413);
        map.put("#3 " + "Name:" + "Allen", 907124321);
        map.put("#4 " + "Name:" + "Kaya", 907123214);
        map.put("#5 " + "Name:" + "Smitty", 907129864);

        for (Map.Entry<String, Integer> map_info :
                map.entrySet()) {
            System.out.print(map_info.getKey() + "  "+ "Phone:");
            System.out.println(map_info.getValue());
        }
        System.out.println("Total in Queue:" + map.size());
        System.out.println("Completed all call backs? " + map.isEmpty());
    }

}
