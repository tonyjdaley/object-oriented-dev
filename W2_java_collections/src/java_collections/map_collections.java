package java_collections;

import org.apache.groovy.json.internal.Value;

import java.util.*;

public class map_collections {
    public static void main(String[] args) {
        Map<String, Integer> map
                = new HashMap<>();
        map.put("matt", 907124532);
        map.put("andrew", 907122413);
        map.put("jared", 907124321);
        map.put("keith", 907123214);
        map.put("alex", 907129864);

        for (Map.Entry<String, Integer> me :
                map.entrySet()) {
            // this print statment gets the "key" values. which is the person name
            System.out.print(me.getKey() + ":");
            // this print statement gets the value of they "key" which is the number
            System.out.println(me.getValue());
        }
        String input = "1234567890";
        String number = input.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3");
        System.out.println(number);
    }
}
    // String input = "1234567890";

    // String number = input.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3");

    // System.out.println(number);
