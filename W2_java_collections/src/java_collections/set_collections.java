package java_collections;

import java.util.*;
import java.util.Set;

public class set_collections {
    public static void main(String[] args) {
        //duplicated items in a set are formed as one and they are not counted in the total list
        Set<Object> set
                = new TreeSet<>();
        set.add("apple");
        set.add("orange");
        set.add("apple");
        set.add("pear");
        set.add("apple");
        set.add("banana");
        set.add("pear");
        System.out.println(set);
        //.size show how many elements there are
        System.out.println(set.size());
        //? not sure how to use this yet
        System.out.println(set.hashCode());
    }
}
